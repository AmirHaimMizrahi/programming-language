CC        = g++
LLVMFLAGS = -I/lib/llvm-10/include/ -L/lib/llvm-10/lib/ $(shell /lib/llvm-10/bin/llvm-config --libs)
CFLAGS	= -O0 -Wall -g -std=c++14 -Iinclude -lm $(LLVMFLAGS)

# -lLLVMDemangle-lrt

DIRS := ${shell find "src" -type d}
SRC  := $(foreach dir,$(DIRS),$(wildcard $(dir)/*.cpp))
OBJ  := $(patsubst src/%.cpp,build/%.o,$(SRC))

EXEC_NAME := debug

#check if compile test file
ifneq ("$(TEST)", "")
SRC := $(SRC) test/$(TEST).cpp
OBJ := $(OBJ) build/$(TEST).o
else
SRC := $(SRC) test/main.cpp
OBJ := $(OBJ) build/main.o
endif

all: build 

ex:
	dist/debug

#link object files
build: $(OBJ)
	@echo Linking...
	@$(CC) $(OBJ) -o dist/$(EXEC_NAME) $(CFLAGS)

#compile src files to object files
build/%.o : src/%.cpp
	@mkdir -p $(@D)
	@echo Compiling - $< 
	@$(CC) -c $< -o $@ $(CFLAGS)

build/%.o : test/%.cpp
	@mkdir -p $(@D)
	@echo Compiling - $< 
	@$(CC) -c $< -o $@ $(CFLAGS)

#run excecuteable
run: build
	@echo Excecuting...
	@./dist/$(EXEC_NAME)

init:
	@mkdir -p build dist include src test

clean:
	@rm -rf build/* dist/*

rebuild: clean build