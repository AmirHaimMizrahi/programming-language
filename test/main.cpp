#include <iostream>
#include "code-generation.hpp"
#include "../include/parser.h"
#include "lexer.h"
#include "token.h"

#include <fstream>
#include <string>

int main()
{
    std::ifstream input_file("test/test.txt");
    std::string content;

    std::getline(input_file, content, (char)EOF);

    Lexer::init(content);
    Parser::init();
    InitialzeModule();

    Lexer::consume();

    try
    {
        do
        {
            std::unique_ptr<ASTNode> node = Parser::consume();
            // std::cout << *node << std::endl;
        } while (Lexer::peek_type() != Token::END_REACHED);

        TheModule->print(errs(), nullptr);
    }
    catch (const std::runtime_error &e)
    {
        std::cerr << e.what() << '\n';
    }

    return 0;
}