#ifndef LEXER_INFO_H
#define LEXER_INFO_H

#include <string>

struct LexerInfo
{
    static std::string current_line;
    static size_t line_number;
    static size_t line_position;
};

#endif