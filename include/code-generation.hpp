#ifndef CODE_GENERATION_H
#define CODE_GENERATION_H

#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Transforms/InstCombine/InstCombine.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Scalar/GVN.h"

#include <memory>
#include <iostream>
#include "token.h"

using namespace llvm;

extern std::unique_ptr<LLVMContext> TheContext;
extern std::unique_ptr<IRBuilder<>> Builder;
extern std::unique_ptr<Module> TheModule;
extern std::unique_ptr<legacy::FunctionPassManager> TheFPM;
extern std::map<std::string, AllocaInst *> variables;
extern std::map<std::string, Argument *> arguments;

void InitialzeModule();

namespace codegen
{
    void create_variable(unique_tok identifier_tok, Type *type, Value *initial_value = nullptr);
    Value *load_variable(const std::string &name);
    void store_variable(const std::string &name, Value *value);
}

template <class T>
T CodeGenerationError(const char *error_log)
{
    std::cerr << error_log << std::endl;
    return nullptr;
}

#endif
