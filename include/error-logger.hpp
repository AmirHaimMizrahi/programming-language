#ifndef ERROR_LOGGER_H
#define ERROR_LOGGER_H

struct ErrorLogger
{
    template <class ASTNodeType>
    static std::unique_ptr<ASTNodeType> ParsingError(const char *error_log)
    {
        std::cerr << error_log << std::endl;
        return nullptr;
    }

    template <class ASTNodeType>
    static std::unique_ptr<ASTNodeType> ParsingError(const char *error_log, unique_tok::pointer tok, bool log_tok_type=false)
    {
        std::cerr << error_log << '\n'
                  << LexerInfo::current_line << '\n';

        for (size_t i = 0; i < tok->line_position; i++)
            std::cerr << ' ';

        for (size_t i = 0, tok_value_size = tok->value.size(); i < tok_value_size; i++)
            std::cerr << '^';

        std::cerr
            << "(pos:" << tok->line_position << " line:" << tok->line_number << ")\n";
        
        if (log_tok_type)   
            std::cerr << "Found " << tok->type_str();

        std::cerr << std::endl;
        return nullptr;
    }
};

#endif