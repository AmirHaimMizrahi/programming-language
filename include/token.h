#ifndef TOKEN_H
#define TOKEN_H

#include <string>
#include "lexer-info.h"

#define unique_tok std::unique_ptr<Token>
#define shared_tok std::shared_ptr<Token>
#define make_unique_tok(...) std::make_unique<Token>(__VA_ARGS__)
#define make_shared_tok(...) std::make_shared<Token>(__VA_ARGS__)

struct Token
{
    enum Type
    {
        END_REACHED = -1,
        NONE,
        IDENTIFIER,
        KEYWORD_VAR,
        KEYWORD_FUNC,
        KEYWORD_REPEAT,
        KEYWORD_USE,
        KEYWORD_RETURN,
        KEYWORD_CASE,
        KEYWORD_ELSE,
        KEYWORD_WITH,
        NUMBER,
        OPERATOR,
        LEFT_PAREN,
        RIGHT_PAREN,
        SEMI_COLON,
        PIPE_OPERATOR,
        COLON,
        LEFT_BRACE,
        RIGHT_BRACE,
        COMMA
    };

    std::string value;
    Type type;
    size_t line_number;
    size_t line_position;

    Token();
    Token(Type type, const std::string &value = "");
    Token(Type type, const std::string &value, size_t line_number, size_t line_position);
    
    const char* type_str();
};

#endif
