#ifndef LEXER_H
#define LEXER_H

#include <iostream>
#include <string>
#include <memory>
#include "token.h"
#include "lexer-info.h"

class Lexer
{
public:
    static void init(std::string buffer);
    static unique_tok consume();
    static unique_tok::pointer peek();
    static Token::Type peek_type();
    static std::string peek_value();

private:
    static inline void advance();
    static void skip_whitespaces();

    static inline bool is_identifier_start();
    static inline bool is_identifier_continue();
    static inline bool is_number_start();
    static inline bool is_number_continue();
    static inline bool is_operator_start();
    static inline bool is_operator_continue();

    static inline Token::Type get_keyword_type(const std::string &tok_value);
    static unique_tok consume_token(bool (*is_continue)(), Token::Type tok_type);

private:
    static std::string::const_iterator cur;
    static std::string::const_iterator end;

    static std::string m_buffer;

    static unique_tok next_tok; // calling consume will lex the next token. calling peek will return this token
};

#endif
