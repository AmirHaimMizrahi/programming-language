#ifndef SCOPE_HPP
#define SCOPE_HPP

#include <vector>
#include <memory>
#include "node.hpp"
#include "statements/statement.hpp"

struct ScopeAST : public ASTNode
{
    std::vector<std::unique_ptr<StmtAST>> statements;

    ScopeAST(std::vector<std::unique_ptr<StmtAST>> &&statements) : statements(std::move(statements)) {}

    void codegen()
    {
        for (size_t i = 0; i < statements.size(); i++)
        {
            statements[i]->codegen();
        }
    }

    std::ostream &print(std::ostream &os) const override
    {
        os << "Scope{\n";
        for (size_t i = 0; i < statements.size(); i++)
        {
            os << *statements[i] << std::endl;
        }
        return os << "}";
    }
};

#endif
