#ifndef FUNCTION_HPP
#define FUNCTION_HPP

#include <vector>
#include <string>
#include "./scope.hpp"
#include "./node.hpp"

struct FunctionAST : public ASTNode
{
    unique_tok identifier_tok;
    std::vector<unique_tok> args;
    std::unique_ptr<ScopeAST> scope;

    FunctionAST(unique_tok identifier_tok, std::vector<unique_tok> &&args, std::unique_ptr<ScopeAST> &&scope) : identifier_tok(std::move(identifier_tok)),
                                                                                                                 args(std::move(args)),
                                                                                                                 scope(std::move(scope)) {}

    Function *codegen()
    {
        Function *function = TheModule->getFunction(identifier_tok->value);
        if (function)
            return CodeGenerationError<Function *>((std::string() + identifier_tok->value + " is already defined").c_str());

        // create function prototype
        std::vector<Type *> doubles(args.size(), Type::getDoubleTy(*TheContext));
        FunctionType *function_type = FunctionType::get(Type::getDoubleTy(*TheContext), doubles, false);
        function = Function::Create(function_type, Function::ExternalLinkage, identifier_tok->value, TheModule.get());

        // create entry block
        BasicBlock *entry_block = BasicBlock::Create(*TheContext, "entry", function);
        Builder->SetInsertPoint(entry_block);

        // initialize function arguments
        variables.clear();
        arguments.clear();
        
        unsigned i = 0;
        for (auto &arg : function->args())
        {
            arg.setName(args[i++]->value);
            arguments[arg.getName().str()] = &arg;
        }

        scope->codegen();

        verifyFunction(*function);
        TheFPM->run(*function);
        return function;
    }

    std::ostream &print(std::ostream &os) const override
    {
        // if (args.size() > 0)
        // {
        //     os << "Function(name: " << name << ", args: (";
        //     os << args[0];

        //     for (size_t i = 1; i < args.size(); i++)
        //     {
        //         os << ", " << args[i];
        //     }

        //     return os << "), scope: " << *scope << ")";
        // }
        // else
        //     return os << "Function(name: " << name << ", scope: " << *scope << ")";
    }
};

#endif
