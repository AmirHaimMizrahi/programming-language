#ifndef OPERATION_HPP
#define OPERATION_HPP

#include <string>
#include <memory>
#include "expression.hpp"

struct OperationExprAST : public ExprAST
{
    unique_tok op_tok;
    std::unique_ptr<ExprAST> lhs;
    std::unique_ptr<ExprAST> rhs;

    OperationExprAST(unique_tok op_tok, std::unique_ptr<ExprAST> &&lhs, std::unique_ptr<ExprAST> &&rhs) : op_tok(std::move(op_tok)),
                                                                                                          lhs(std::move(lhs)),
                                                                                                          rhs(std::move(rhs)) {}

    Value *codegen()
    {
        Value *L = lhs->codegen();
        Value *R = rhs->codegen();

        if (!L || !R)
            return nullptr;

        if (op_tok->value == "+")
            return Builder->CreateFAdd(L, R, "addtmp");
        if (op_tok->value == "-")
            return Builder->CreateFSub(L, R, "subtmp");
        if (op_tok->value == "*")
            return Builder->CreateFMul(L, R, "multmp");
        if (op_tok->value == "/")
            return Builder->CreateFDiv(L, R, "divtmp");
        if (op_tok->value == "==")
        {
            L = Builder->CreateFCmpUEQ(L, R, "eqtmp");
            return Builder->CreateUIToFP(L, Type::getDoubleTy(*TheContext), "booltmp");
        }
        return CodeGenerationError<Value *>("invalid binary operator");
    }

    std::ostream &print(std::ostream &os) const override
    {
        // return os << "(" << *lhs << ' ' << op << ' ' << *rhs << ")";
    }
};

#endif
