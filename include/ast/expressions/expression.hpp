#ifndef EXPRESSION_HPP
#define EXPRESSION_HPP

#include "../node.hpp"

struct ExprAST : public ASTNode
{
    virtual ~ExprAST() = default;
    virtual Value *codegen() = 0;

    std::ostream &print(std::ostream &os) const override
    {
        return os << "Expression";
    }
};

#endif
