#ifndef VARIABLE_HPP
#define VARIABLE_HPP

#include <string>
#include "expression.hpp"

struct VariableExprAST : public ExprAST
{
    unique_tok identifier_tok;

    VariableExprAST(unique_tok identifier_tok) : identifier_tok(std::move(identifier_tok))
    {
    }

    Value *codegen()
    {
        return codegen::load_variable(identifier_tok->value);
    }

    std::ostream &print(std::ostream &os) const override
    {
        // return os << "Variable(" << name << ")";
    }
};

#endif
