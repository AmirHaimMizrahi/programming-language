#ifndef NUMBER_HPP
#define NUMBER_HPP

#include <string>
#include "expression.hpp"

struct NumberExprAST : public ExprAST
{
    unique_tok number_tok;

    NumberExprAST(unique_tok number_tok) : number_tok(std::move(number_tok)) {}

    Value *codegen()
    {
        return ConstantFP::get(*TheContext, APFloat(std::stod(number_tok->value)));
    }

    std::ostream &print(std::ostream &os) const override
    {
        // return os << "Number(" << number << ")";
    }
};

#endif
