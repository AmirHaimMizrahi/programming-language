#ifndef FUNCTION_CALL_HPP
#define FUNCTION_CALL_HPP

#include <string>
#include <memory>
#include <vector>
#include <iostream>
#include "expression.hpp"

struct FunctionCallExprAST : public ExprAST
{
    unique_tok identifier_tok;
    std::vector<std::unique_ptr<ExprAST>> args;

    FunctionCallExprAST(unique_tok identifier_tok, std::vector<std::unique_ptr<ExprAST>> &&args) : identifier_tok(std::move(identifier_tok)),
                                                                                                   args(std::move(args)) {}

    Value *codegen()
    {
        Function *function = TheModule->getFunction(identifier_tok->value);
        if (!function)
            return CodeGenerationError<Value *>((std::string() + '"' + identifier_tok->value + '"' + " is an unknown function").c_str());

        if (function->arg_size() != args.size())
        {
            if (function->arg_size() > args.size())
                return CodeGenerationError<Value *>((identifier_tok->value + " missing " + std::to_string(function->arg_size() - args.size()) + " arguments").c_str());
            else
                return CodeGenerationError<Value *>((identifier_tok->value + " requires " + std::to_string(function->arg_size()) + " arguments but " + std::to_string(args.size()) + " were given").c_str());
        }
        std::vector<Value *> function_args;
        for (unsigned i = 0, e = args.size(); i != e; ++i)
        {
            function_args.push_back(args[i]->codegen());
            if (!function_args.back())
                return nullptr;
        }

        return Builder->CreateCall(function, function_args, "calltmp");
    }

    std::ostream &print(std::ostream &os) const override
    {
        // os << "FunctionCall(name: " << function_name << ", args: (";
        // os << *args[0];

        // for (size_t i = 1; i < args.size(); i++)
        // {
        //     os << ", " << *args[i];
        // }
        // return os << ")";
    }
};

#endif
