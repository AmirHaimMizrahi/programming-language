#ifndef RETURN_STATEMENT_HPP
#define RETURN_STATEMENT_HPP

#include <string>
#include <memory>
#include "../expressions/expression.hpp"
#include "statement.hpp"

struct ReturnStmt : public StmtAST
{
    std::unique_ptr<ExprAST> value;

    ReturnStmt(std::unique_ptr<ExprAST> &&value) : value(std::move(value)) {}

    void codegen()
    {
        Builder->CreateRet(value->codegen());
    }

    std::ostream &print(std::ostream &os) const override
    {
        return os << "Return(value: " << *value << ")";
    }
};

#endif
