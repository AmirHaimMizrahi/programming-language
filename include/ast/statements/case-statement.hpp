#ifndef CASE_STATEMENT_HPP
#define CASE_STATEMENT_HPP

#include <vector>
#include <memory>
#include "../expressions/expression.hpp"
#include "../scope.hpp"
#include "statement.hpp"

struct CaseStmt : public StmtAST
{
    std::vector<std::unique_ptr<ExprAST>> conditions;
    std::vector<std::unique_ptr<ScopeAST>> scopes;
    std::unique_ptr<ScopeAST> else_scope;

    CaseStmt(
        std::vector<std::unique_ptr<ExprAST>> &&conditions,
        std::vector<std::unique_ptr<ScopeAST>> &&scopes,
        std::unique_ptr<ScopeAST> &&else_scope) : conditions(std::move(conditions)),
                                                  scopes(std::move(scopes)),
                                                  else_scope(std::move(else_scope)) {}

    void codegen()
    {
        size_t conditions_size = conditions.size();

        // declare blocks
        Function *parent_function = Builder->GetInsertBlock()->getParent();
        std::vector<BasicBlock *> condition_blocks;
        std::vector<BasicBlock *> scope_blocks;
        BasicBlock *else_block;
        BasicBlock *case_after_block;

        // initialize condition and case blocks
        for (size_t i = 0; i < conditions_size; i++)
        {
            BasicBlock *case_condition = BasicBlock::Create(*TheContext, "case_condition", parent_function);
            condition_blocks.push_back(case_condition);

            BasicBlock *case_scope = BasicBlock::Create(*TheContext, "case_scope", parent_function);
            scope_blocks.push_back(case_scope);
        }

        // enter first case statement
        Builder->CreateBr(condition_blocks[0]);

        // initialize else block and after block
        if (else_scope)
            else_block = BasicBlock::Create(*TheContext, "else_scope", parent_function);
        case_after_block = BasicBlock::Create(*TheContext, "case_after", parent_function);

        // initialize scope blocks
        for (size_t i = 0; i < conditions_size; i++)
        {
            Builder->SetInsertPoint(scope_blocks[i]);
            scopes[i]->codegen();
            Builder->CreateBr(case_after_block);
        }

        // initialize else scope
        if (else_scope)
        {
            Builder->SetInsertPoint(else_block);
            else_scope->codegen();
            Builder->CreateBr(case_after_block);
        }

        // initialize condition blocks
        for (size_t i = 0; i < conditions_size - 1; i++)
        {
            Builder->SetInsertPoint(condition_blocks[i]);
            Value *condition = conditions[i]->codegen();
            Value *cmp = Builder->CreateFCmp(CmpInst::Predicate::FCMP_ONE, condition, ConstantFP::get(Type::getDoubleTy(*TheContext), 0), "next_case_cmp");
            Builder->CreateCondBr(cmp, scope_blocks[i], condition_blocks[i + 1]);
        }

        // initialize last condition block (should jump to else block)
        Builder->SetInsertPoint(condition_blocks[conditions_size - 1]);
        Value *condition = conditions[conditions_size - 1]->codegen();
        Value *cmp = Builder->CreateFCmp(CmpInst::Predicate::FCMP_ONE, condition, ConstantFP::get(Type::getDoubleTy(*TheContext), 0), "next_case_cmp");
        if (else_scope)
            Builder->CreateCondBr(cmp, scope_blocks[conditions_size - 1], else_block);
        else
            Builder->CreateCondBr(cmp, scope_blocks[conditions_size - 1], case_after_block);

        Builder->SetInsertPoint(case_after_block);
    }

    std::ostream &print(std::ostream &os) const override
    {
        return os;
        // return os << "Return(value: " << *value << ")";
    }
};

#endif
