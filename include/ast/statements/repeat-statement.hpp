#ifndef REPEAT_STATEMENT_HPP
#define REPEAT_STATEMENT_HPP

#include <string>
#include <memory>
#include "../expressions/expression.hpp"
#include "../expressions/variable.hpp"
#include "../scope.hpp"
#include "statement.hpp"

struct RepeatStmt : public StmtAST
{
    std::unique_ptr<ExprAST> count;
    unique_tok identifier_tok;
    std::unique_ptr<ScopeAST> scope;

    RepeatStmt(std::unique_ptr<ExprAST> &&count, unique_tok identifier_tok, std::unique_ptr<ScopeAST> &&scope) : count(std::move(count)),
                                                                                                                 identifier_tok(std::move(identifier_tok)),
                                                                                                                 scope(std::move(scope)) {}

    void codegen()
    {
        // initialize repeat and after_repeat blocks
        Function *parent_function = Builder->GetInsertBlock()->getParent();
        BasicBlock *repeat_block = BasicBlock::Create(*TheContext, "repeat", parent_function);
        BasicBlock *repeat_after_block = BasicBlock::Create(*TheContext, "repeat_after", parent_function);

        // initialize repeat counter
        if (identifier_tok->value.empty())
            identifier_tok->value = "repeat_counter";
        codegen::create_variable(std::move(identifier_tok), Type::getDoubleTy(*TheContext), ConstantFP::get(Type::getDoubleTy(*TheContext), 0));

        // enter repeat condition
        Value *counter_value = codegen::load_variable(identifier_tok->value);
        Value *cmp = Builder->CreateFCmp(CmpInst::Predicate::FCMP_OLT, counter_value, count->codegen(), "enter_repeat_cmp");
        Builder->CreateCondBr(cmp, repeat_block, repeat_after_block);

        // repeat body
        Builder->SetInsertPoint(repeat_block);
        scope->codegen();

        // increment counter
        Value *increment_counter = Builder->CreateAdd(counter_value, ConstantFP::get(Type::getDoubleTy(*TheContext), 1), "inc_repeat_counter");
        codegen::store_variable(identifier_tok->value, increment_counter);

        // continue repeat condition
        counter_value = codegen::load_variable(identifier_tok->value);
        cmp = Builder->CreateFCmp(CmpInst::Predicate::FCMP_OLT, counter_value, count->codegen(), "continue_repeat_cmp");
        Builder->CreateCondBr(cmp, repeat_block, repeat_after_block);

        Builder->SetInsertPoint(repeat_after_block);
    }

    std::ostream &print(std::ostream &os) const override
    {
        // if (!identifier_tok->value.empty())
        //     return os << "RepeatStmt(count: " << *count << ", counter_name: " << counter_name << ", scope: " << *scope << ")";
        // else
        //     return os << "RepeatStmt(count: " << *count << ", scope: " << *scope << ")";
    }
};

#endif
