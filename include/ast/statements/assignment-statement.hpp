#ifndef ASSIGNMENT_STATEMENT_HPP
#define ASSIGNMENT_STATEMENT_HPP

#include <string>
#include <memory>
#include "../expressions/expression.hpp"
#include "statement.hpp"

struct AssignmentStmt : public StmtAST
{
    unique_tok identifier_tok;
    std::unique_ptr<ExprAST> value;

    AssignmentStmt(unique_tok identifier_tok, std::unique_ptr<ExprAST> &&value) : identifier_tok(std::move(identifier_tok)),
                                                                                  value(std::move(value)) {}

    void codegen()
    {
        codegen::store_variable(identifier_tok->value, value->codegen());
    }

    std::ostream &print(std::ostream &os) const override
    {
        // return os << "Assignment(variable_name: " << variable_name << ", value: " << *value << ")";
    }
};

#endif
