#ifndef STATEMENT_HPP
#define STATEMENT_HPP

#include "../node.hpp"

struct StmtAST : public ASTNode
{
    virtual ~StmtAST() = default;

    virtual void codegen() = 0;

    std::ostream &print(std::ostream &os) const override
    {
        return os << "Statement";
    }
};

#endif
