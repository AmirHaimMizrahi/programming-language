#ifndef VARIABLE_DECLARATION_STATEMENT_HPP
#define VARIABLE_DECLARATION_STATEMENT_HPP

#include <string>
#include <memory>
#include "../expressions/expression.hpp"
#include "statement.hpp"

struct VariableDeclarationStmt : public StmtAST
{
    unique_tok identifier_tok;
    std::unique_ptr<ExprAST> initial_value;

    VariableDeclarationStmt(unique_tok identifier_tok, std::unique_ptr<ExprAST> &&initial_value) : identifier_tok(std::move(identifier_tok)),
                                                                                                   initial_value(std::move(initial_value)) {}

    void codegen()
    {
        codegen::create_variable(std::move(identifier_tok), Type::getDoubleTy(*TheContext), initial_value ? initial_value->codegen() : nullptr);
    }

    std::ostream &print(std::ostream &os) const override
    {
        // if (initial_value)
        //     return os << "VariableDeclarationStmt(name: " << name << ", initial_value: " << *initial_value << ")";
        // else
        //     return os << "VariableDeclarationStmt(name: " << name << ")";
    }
};

#endif
