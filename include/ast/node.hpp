#ifndef NODE_HPP
#define NODE_HPP

#include <iostream>
#include "../code-generation.hpp"

struct ASTNode
{
    virtual ~ASTNode() = default;

    virtual std::ostream &print(std::ostream &os) const
    {
        return os << "ASTNode";
    }

    friend std::ostream &operator<<(std::ostream &os, const ASTNode &node)
    {
        return node.print(os);
    }
};

#endif
