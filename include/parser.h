#ifndef PARSER_H
#define PARSER_H

#include <memory>
#include <iostream>
#include <unordered_map>
#include "lexer.h"
#include "error-logger.hpp"
#include "ast/node.hpp"
#include "ast/expressions/expression.hpp"
#include "ast/expressions/number.hpp"
#include "ast/expressions/variable.hpp"
#include "ast/expressions/function-call.hpp"
#include "ast/expressions/operation.hpp"
#include "ast/statements/statement.hpp"
#include "ast/statements/variable-declaration-statement.hpp"
#include "ast/statements/repeat-statement.hpp"
#include "ast/statements/return-statement.hpp"
#include "ast/statements/assignment-statement.hpp"
#include "ast/statements/case-statement.hpp"
#include "ast/scope.hpp"
#include "ast/function.hpp"

/*
    var randomNumber = get_random_number(0, 100) ~ random_number_generator() ~  seed() ~ time
    var randomNumber = get_random_number with 0, 100 ~ random_number_generator() ~  seed() ~ time
    
    CalcFunction ~ RandomNumber ~ GetSeed ~ n
    n ~ GetSeed ~ RandomNumber ~ CalcFunction

    str ~ Upper() ~ Reverse() ~ Split use a, b and c
*/

class Parser
{
public:
    static void init();
    static std::unique_ptr<ASTNode> consume();

private:
    static std::unique_ptr<ScopeAST> consume_scope();
    static std::unique_ptr<FunctionAST> consume_function();

    // statements
    static std::unique_ptr<StmtAST> consume_stmt();
    static std::unique_ptr<StmtAST> consume_variable_declaration_stmt();
    static std::unique_ptr<StmtAST> consume_repeat_stmt();
    static std::unique_ptr<StmtAST> consume_return_stmt();
    static std::unique_ptr<StmtAST> consume_assignment_stmt();
    static std::unique_ptr<StmtAST> consume_case_stmt();

    // expressions
    static int peek_op_precedence();
    static std::unique_ptr<ExprAST> consume_expr();
    static std::unique_ptr<ExprAST> consume_primary();
    static std::unique_ptr<ExprAST> consume_identifier_expr();
    static std::unique_ptr<ExprAST> consume_number_expr();
    static std::unique_ptr<ExprAST> consume_operation_expr(std::unique_ptr<ExprAST> lhs, const int min_precedence = 0);
    static std::unique_ptr<ExprAST> consume_paren_expr();
};

#endif
