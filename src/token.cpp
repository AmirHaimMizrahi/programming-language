#include "token.h"

Token::Token() : line_number(LexerInfo::line_number),
                 line_position(LexerInfo::line_position) {}

Token::Token(Type type, const std::string &value) : type(type),
                                                    value(value),
                                                    line_number(LexerInfo::line_number),
                                                    line_position(LexerInfo::line_position) {}

Token::Token(Type type, const std::string &value, size_t line_number, size_t line_position) : type(type),
                                                                                              value(value),
                                                                                              line_number(line_number),
                                                                                              line_position(line_position)
{
}

const char *Token::type_str()
{
    switch (type)
    {
    case END_REACHED:
        return "END_REACHED";
    case NONE:
        return "NONE";
    case IDENTIFIER:
        return "IDENTIFIER";
    case KEYWORD_VAR:
        return "KEYWORD_VAR";
    case KEYWORD_FUNC:
        return "KEYWORD_FUNC";
    case KEYWORD_REPEAT:
        return "KEYWORD_REPEAT";
    case KEYWORD_USE:
        return "KEYWORD_USE";
    case KEYWORD_RETURN:
        return "KEYWORD_RETURN";
    case KEYWORD_CASE:
        return "KEYWORD_CASE";
    case KEYWORD_ELSE:
        return "KEYWORD_ELSE";
    case KEYWORD_WITH:
        return "KEYWORD_WITH";
    case NUMBER:
        return "NUMBER";
    case OPERATOR:
        return "OPERATOR";
    case LEFT_PAREN:
        return "LEFT_PAREN";
    case RIGHT_PAREN:
        return "RIGHT_PAREN";
    case SEMI_COLON:
        return "SEMI_COLON";
    case COLON:
        return "COLON";
    case PIPE_OPERATOR:
        return "PIPE_OPERATOR";
    case LEFT_BRACE:
        return "LEFT_BRACE";
    case RIGHT_BRACE:
        return "RIGHT_BRACE";
    case COMMA:
        return "COMMA";
    }
}