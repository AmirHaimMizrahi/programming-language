#include "lexer-info.h"

size_t LexerInfo::line_number;
size_t LexerInfo::line_position;
std::string LexerInfo::current_line;