#include "../include/parser.h"

void Parser::init()
{
}

std::unique_ptr<ASTNode> Parser::consume()
{
    if (Lexer::peek_type() == Token::END_REACHED)
        return nullptr;

    if (Lexer::peek_type() == Token::KEYWORD_FUNC)
    {
        auto function_ast = consume_function();
        function_ast->codegen();
        return function_ast;
    }
    auto scope_ast = consume_scope();
    scope_ast->codegen();
    return scope_ast;
}

std::unique_ptr<ScopeAST> Parser::consume_scope()
{
    std::vector<std::unique_ptr<StmtAST>> statements;

    if (Lexer::peek_type() != Token::LEFT_BRACE)
    {
        auto statement = consume_stmt();
        if (!statement)
            return ErrorLogger::ParsingError<ScopeAST>("an error occured while parsing statement for a braceless scope");

        statements.push_back(std::move(statement));
        return std::make_unique<ScopeAST>(std::move(statements));
    }

    Lexer::consume(); // consume left brace

    while (Lexer::peek_type() != Token::RIGHT_BRACE)
    {
        auto stmt = consume_stmt();
        if (!stmt)
            return ErrorLogger::ParsingError<ScopeAST>("an error occured while parsing a statement for a scope");
        statements.push_back(std::move(stmt));
    }
    Lexer::consume(); // consume right brace
    return std::make_unique<ScopeAST>(std::move(statements));
}

std::unique_ptr<FunctionAST> Parser::consume_function()
{
    Lexer::consume(); // consume function keyword

    if (Lexer::peek_type() != Token::IDENTIFIER)
        return ErrorLogger::ParsingError<FunctionAST>("expected an identifier for function name", Lexer::peek(), true);

    auto identifier_tok = Lexer::consume();

    std::vector<unique_tok> args;

    if (Lexer::peek_type() == Token::COLON)
    {
        Lexer::consume(); // consume colon

        if (Lexer::peek_type() != Token::IDENTIFIER)
            return ErrorLogger::ParsingError<FunctionAST>("expected identifier after ':' for function arguments list", Lexer::peek(), true);

        while (true)
        {
            if (Lexer::peek_type() != Token::IDENTIFIER)
                return ErrorLogger::ParsingError<FunctionAST>("expected identifier after ',' for function arguments list", Lexer::peek(), true);

            auto argument_identifier_tok = Lexer::consume();
            args.push_back(std::move(argument_identifier_tok));

            if (Lexer::peek_type() != Token::COMMA)
                break;

            Lexer::consume(); // consume comma
        }
    }

    auto scope = consume_scope();
    if (!scope)
        return ErrorLogger::ParsingError<FunctionAST>("an error occured while parsing a scope for a function body");

    return std::make_unique<FunctionAST>(std::move(identifier_tok), std::move(args), std::move(scope));
}

// statements

std::unique_ptr<StmtAST> Parser::consume_stmt()
{
    switch (Lexer::peek_type())
    {
    case Token::KEYWORD_VAR:
        return consume_variable_declaration_stmt();

    case Token::KEYWORD_REPEAT:
        return consume_repeat_stmt();

    case Token::KEYWORD_RETURN:
        return consume_return_stmt();

    case Token::IDENTIFIER:
        return consume_assignment_stmt();

    case Token::KEYWORD_CASE:
        return consume_case_stmt();

    default:
        return ErrorLogger::ParsingError<StmtAST>("unexpected token when parsing statement", Lexer::peek(), true);
    }
}

std::unique_ptr<StmtAST> Parser::consume_variable_declaration_stmt()
{
    Lexer::consume(); // consume keyword var

    if (Lexer::peek_type() != Token::IDENTIFIER)
        return ErrorLogger::ParsingError<VariableDeclarationStmt>("expected an identifier for variable name", Lexer::peek(), true);

    auto identifier_tok = Lexer::consume();

    if (Lexer::peek_value() == "=")
    {
        Lexer::consume(); // consume '='

        auto initial_value = consume_expr();
        if (!initial_value)
            return ErrorLogger::ParsingError<VariableDeclarationStmt>("an error occured while parsing initial value for a variable declaration statement");

        return std::make_unique<VariableDeclarationStmt>(std::move(identifier_tok), std::move(initial_value));
    }

    return std::make_unique<VariableDeclarationStmt>(std::move(identifier_tok), nullptr);
}

std::unique_ptr<StmtAST> Parser::consume_repeat_stmt()
{
    Lexer::consume(); // consume repeat keyword

    // repeat count
    auto count_expr = consume_expr();
    if (!count_expr)
        return ErrorLogger::ParsingError<RepeatStmt>("an error occured while parsing the repeat statement count expression");

    unique_tok identifier_tok = nullptr;

    if (Lexer::peek_type() == Token::KEYWORD_USE)
    {
        Lexer::consume(); // consume use keyword

        if (Lexer::peek_type() != Token::IDENTIFIER)
            return ErrorLogger::ParsingError<RepeatStmt>("expected an identifier to use as a repeat statement counter", Lexer::peek(), true);

        identifier_tok = Lexer::consume();
    }

    auto scope = consume_scope();
    if (!scope)
        return ErrorLogger::ParsingError<RepeatStmt>("an error occured while parsing scope for a repeat statement");

    return std::make_unique<RepeatStmt>(std::move(count_expr), std::move(identifier_tok), std::move(scope));
}

std::unique_ptr<StmtAST> Parser::consume_return_stmt()
{
    Lexer::consume(); // consume return keyword

    auto return_expr = consume_expr();
    if (!return_expr)
        return ErrorLogger::ParsingError<ReturnStmt>("an error occured while parsing expression for a return statement");

    return std::make_unique<ReturnStmt>(std::move(return_expr));
}

std::unique_ptr<StmtAST> Parser::consume_assignment_stmt()
{
    auto identifier_tok = Lexer::consume();

    if (Lexer::peek_value() != "=")
        return ErrorLogger::ParsingError<AssignmentStmt>("expected '=' after identifier for variable assignment statement", Lexer::peek(), true);

    Lexer::consume(); // consume '='

    auto expr = consume_expr();
    if (!expr)
        return ErrorLogger::ParsingError<AssignmentStmt>("an error occured while parsing expression for variable assignment statement");

    return std::make_unique<AssignmentStmt>(std::move(identifier_tok), std::move(expr));
}

std::unique_ptr<StmtAST> Parser::consume_case_stmt()
{
    Lexer::consume(); // consume case keyword

    std::vector<std::unique_ptr<ExprAST>> conditions;
    std::vector<std::unique_ptr<ScopeAST>> scopes;
    std::unique_ptr<ScopeAST> else_scope = nullptr;

    while (true)
    {
        // consume condition
        auto condition = consume_expr();
        if (!condition)
            return ErrorLogger::ParsingError<CaseStmt>("an error occured while parsing a condition for a case statement");
        conditions.push_back(std::move(condition));

        // consume scope
        auto scope = consume_scope();
        if (!scope)
            return ErrorLogger::ParsingError<CaseStmt>("an error occured while parsing a scope for a case statement");
        scopes.push_back(std::move(scope));

        if (Lexer::peek_type() == Token::KEYWORD_ELSE)
        {
            Lexer::consume(); // consume else keyword

            auto scope = consume_scope();
            if (!scope)
                return ErrorLogger::ParsingError<CaseStmt>("an error occured while parsing a scope for a case statement");

            else_scope = std::move(scope);
            break;
        }

        if (Lexer::peek_type() != Token::KEYWORD_CASE)
            break;

        Lexer::consume(); // consume case keyword
    }
    return std::make_unique<CaseStmt>(std::move(conditions), std::move(scopes), std::move(else_scope));
}

// expressions

std::unique_ptr<ExprAST> Parser::consume_expr()
{
    auto lhs = consume_primary();
    if (!lhs)
        return nullptr;
    // return ErrorLogger::ParsingError<ExprAST>("an error occured while paring a value for an expression");

    return std::move(consume_operation_expr(std::move(lhs)));
}

int Parser::peek_op_precedence()
{
    if (Lexer::peek_type() != Token::OPERATOR)
        return -1;

    static const std::unordered_map<std::string, int> operations_precedence = {
        {"==", 1},
        {"+", 2},
        {"-", 2},
        {"*", 3},
        {"/", 3},
    };

    return operations_precedence.at(Lexer::peek_value());
}

std::unique_ptr<ExprAST> Parser::consume_primary()
{
    switch (Lexer::peek_type())
    {
    case Token::NUMBER:
        return consume_number_expr();

    case Token::IDENTIFIER:
        return consume_identifier_expr();

    case Token::LEFT_PAREN:
        return consume_paren_expr();

    default:
        return ErrorLogger::ParsingError<ExprAST>("unexpected token when parsing an expression", Lexer::peek(), true);
    }
}

std::unique_ptr<ExprAST> Parser::consume_identifier_expr()
{
    auto identifier_tok = Lexer::consume();

    std::vector<std::unique_ptr<ExprAST>> args;

    // function call with 'with' keyword
    if (Lexer::peek_type() == Token::KEYWORD_WITH)
    {
        Lexer::consume(); // consume with keyword

        while (true)
        {
            auto expr = consume_expr();
            if (!expr)
                return ErrorLogger::ParsingError<FunctionCallExprAST>("an error occured while parsing an expression for a function call arguments list");
            args.push_back(std::move(expr));

            if (Lexer::peek_type() != Token::COMMA)
                break;

            Lexer::consume(); // consume comma
        }

        if (Lexer::peek_type() == Token::PIPE_OPERATOR)
        {
            Lexer::consume(); // consume pipe operator

            auto expr = consume_expr();
            if (!expr)
                return ErrorLogger::ParsingError<FunctionCallExprAST>("an error occured while parsing an expression to pipe to a function call");
            args.push_back(std::move(expr));
        }

        return std::make_unique<FunctionCallExprAST>(std::move(identifier_tok), std::move(args));
    }
    // function call with parenthesis
    else if (Lexer::peek_type() == Token::LEFT_PAREN)
    {
        Lexer::consume(); // consume left paren

        // if right paren, no arguments
        if (Lexer::peek_type() == Token::RIGHT_PAREN)
        {
            Lexer::consume(); // consume right paren

            if (Lexer::peek_type() == Token::PIPE_OPERATOR)
            {
                Lexer::consume(); // consume pipe operator

                auto expr = consume_expr();
                if (!expr)
                    return ErrorLogger::ParsingError<FunctionCallExprAST>("an error occured while parsing an expression to pipe to a function call");
                args.push_back(std::move(expr));
            }

            return std::make_unique<FunctionCallExprAST>(std::move(identifier_tok), std::move(args));
        }

        while (true)
        {
            auto expr = consume_expr();
            if (!expr)
                return ErrorLogger::ParsingError<FunctionCallExprAST>("an error occured while parsing an expression for a function call arguments list");
            args.push_back(std::move(expr));

            // if right paren, no more arguments
            if (Lexer::peek_type() == Token::RIGHT_PAREN)
            {
                Lexer::consume(); // consume right paren
                break;
            }

            if (Lexer::peek_type() != Token::COMMA)
                return ErrorLogger::ParsingError<FunctionCallExprAST>("expected ')' at end of a function call\n or\nexpected ',' to pass another argument", Lexer::peek(), true);

            Lexer::consume(); // consume comma
        }

        if (Lexer::peek_type() == Token::PIPE_OPERATOR)
        {
            Lexer::consume(); // consume pipe operator

            auto expr = consume_expr();
            if (!expr)
                return ErrorLogger::ParsingError<FunctionCallExprAST>("an error occured while parsing an expression to pipe to a function call");
            args.push_back(std::move(expr));
        }

        return std::make_unique<FunctionCallExprAST>(std::move(identifier_tok), std::move(args));
    }
    // no function call, just variable
    else
    {
        return std::make_unique<VariableExprAST>(std::move(identifier_tok));
    }
}

std::unique_ptr<ExprAST> Parser::consume_number_expr()
{
    auto number_tok = Lexer::consume();
    return std::make_unique<NumberExprAST>(std::move(number_tok));
}

std::unique_ptr<ExprAST> Parser::consume_operation_expr(std::unique_ptr<ExprAST> lhs, const int min_precedence)
{
    int op_precedence = peek_op_precedence();

    while (op_precedence >= min_precedence)
    {
        auto op_tok = Lexer::consume();

        auto rhs = consume_primary();
        if (!rhs)
            return ErrorLogger::ParsingError<ExprAST>("an error occured while paring a value for an operation expression");

        int next_op_precedence = peek_op_precedence();

        while (next_op_precedence >= op_precedence)
        {
            if (next_op_precedence > op_precedence)
                rhs = consume_operation_expr(std::move(rhs), op_precedence + 1);
            else
                rhs = consume_operation_expr(std::move(rhs), op_precedence);

            if (!rhs)
                return ErrorLogger::ParsingError<ExprAST>("an error occured while paring a value for an operation expression");

            next_op_precedence = peek_op_precedence();
        }
        lhs = std::make_unique<OperationExprAST>(std::move(op_tok), std::move(lhs), std::move(rhs));

        op_precedence = peek_op_precedence();
    }
    return lhs;
}

std::unique_ptr<ExprAST> Parser::consume_paren_expr()
{
    Lexer::consume(); // consume left paren;

    auto expression = consume_expr();
    if (!expression)
        return ErrorLogger::ParsingError<ExprAST>("an error occured while paring an expression for a parentheses expression");

    if (Lexer::peek_type() != Token::RIGHT_PAREN)
        return ErrorLogger::ParsingError<ExprAST>("expected ')' to close parenthesis expression", Lexer::peek(), true);

    Lexer::consume(); // consume right paren
    return expression;
}