#include "lexer.h"

std::string::const_iterator Lexer::cur;
std::string::const_iterator Lexer::end;
std::string Lexer::m_buffer;
unique_tok Lexer::next_tok;

void Lexer::init(std::string buffer)
{
    m_buffer = std::move(buffer);
    cur = m_buffer.begin();
    end = m_buffer.end();
    LexerInfo::current_line += *cur;
    LexerInfo::line_number = 1;
    LexerInfo::line_position = 1;
}

unique_tok Lexer::consume()
{
    skip_whitespaces();

    unique_tok last_tok = std::move(next_tok);
    if (cur == end)
    {
        next_tok = make_unique_tok(Token::END_REACHED);
        return std::move(last_tok);
    }

    if (is_identifier_start())
    {
        next_tok = consume_token(is_identifier_continue, Token::IDENTIFIER);
        Token::Type keyword_type = get_keyword_type(next_tok->value);

        if (keyword_type != Token::NONE)
            next_tok->type = keyword_type;

        return std::move(last_tok);
    }

    if (is_number_start())
    {
        next_tok = consume_token(is_number_continue, Token::NUMBER);
        return std::move(last_tok);
    }
    if (is_operator_start())
    {
        next_tok = consume_token(is_operator_continue, Token::OPERATOR);
        return std::move(last_tok);
    }
    switch (*cur)
    {
    case '(':
    {
        next_tok = consume_token([]
                                 { return false; },
                                 Token::LEFT_PAREN);
        return std::move(last_tok);
    }
    case ')':
    {
        next_tok = consume_token([]
                                 { return false; },
                                 Token::RIGHT_PAREN);
        return std::move(last_tok);
    }

    case '{':
    {
        next_tok = consume_token([]
                                 { return false; },
                                 Token::LEFT_BRACE);
        return std::move(last_tok);
    }

    case '}':
    {
        next_tok = consume_token([]
                                 { return false; },
                                 Token::RIGHT_BRACE);
        return std::move(last_tok);
    }

    case ';':
    {
        next_tok = consume_token([]
                                 { return false; },
                                 Token::SEMI_COLON);
        return std::move(last_tok);
    }

    case ':':
    {
        next_tok = consume_token([]
                                 { return false; },
                                 Token::COLON);
        return std::move(last_tok);
    }

    case ',':
    {
        next_tok = consume_token([]
                                 { return false; },
                                 Token::COMMA);
        return std::move(last_tok);
    }

    case '~':
    {
        next_tok = consume_token([]
                                 { return false; },
                                 Token::PIPE_OPERATOR);
        return std::move(last_tok);
    }
    }

    // std::string error_msg;
    // error_msg += std::string() + "(pos:" + std::to_string(line_position) + " line:" + std::to_string(line_number) + ") " + "unknown symbol '" + (char)(*cur) + "\'\n";

    // error_msg += current_line + '\n';

    // for (size_t i = 0; i < line_position; i++)
    // {
    //     error_msg += ' ';
    // }
    // error_msg += "^";
    // advance();
    // throw std::runtime_error(error_msg);
}

unique_tok::pointer Lexer::peek()
{
    return next_tok.get();
}

Token::Type Lexer::peek_type()
{
    return next_tok->type;
}

std::string Lexer::peek_value()
{
    return next_tok->value;
}

Token::Type Lexer::get_keyword_type(const std::string &tok_value)
{
    if (tok_value == "var")
        return Token::KEYWORD_VAR;

    else if (tok_value == "repeat")
        return Token::KEYWORD_REPEAT;

    else if (tok_value == "use")
        return Token::KEYWORD_USE;

    else if (tok_value == "func")
        return Token::KEYWORD_FUNC;

    else if (tok_value == "return")
        return Token::KEYWORD_RETURN;

    else if (tok_value == "case")
        return Token::KEYWORD_CASE;

    else if (tok_value == "else")
        return Token::KEYWORD_ELSE;

    else if (tok_value == "with")
        return Token::KEYWORD_WITH;

    else
        return Token::NONE;
}

void Lexer::advance()
{
    if (*cur == '\n')
    {
        LexerInfo::line_position = -1;
        LexerInfo::line_number++;
        LexerInfo::current_line.clear();
    }
    ++cur;
    LexerInfo::current_line += *cur;
    LexerInfo::line_position++;
}

unique_tok Lexer::consume_token(bool (*is_continue)(), Token::Type tok_type)
{
    unique_tok tok = make_unique_tok();
    tok->type = tok_type;

    tok->value += *cur;
    advance();

    while (is_continue())
    {
        tok->value += *cur;
        advance();
    }

    return std::move(tok);
}

void Lexer::skip_whitespaces()
{
    while (std::isspace(*cur))
    {
        advance();
    }
}

bool Lexer::is_identifier_start()
{
    return std::isalpha(*cur);
}

bool Lexer::is_identifier_continue()
{
    return std::isalnum(*cur) || *cur == '_';
}

bool Lexer::is_number_start()
{
    return *cur >= '1' && *cur <= '9';
}

bool Lexer::is_number_continue()
{
    return std::isdigit(*cur) || *cur == '_';
}

bool Lexer::is_operator_start()
{
    switch (*cur)
    {
    case '+':
    case '-':
    case '*':
    case '/':
    case '=':
        return true;
    default:
        return false;
    }
}

bool Lexer::is_operator_continue()
{
    switch (*cur)
    {
    case '=':
        return true;
    default:
        return false;
    }
}
