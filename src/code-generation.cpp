#include "code-generation.hpp"

std::unique_ptr<LLVMContext> TheContext;
std::unique_ptr<IRBuilder<>> Builder;
std::unique_ptr<Module> TheModule;
std::unique_ptr<legacy::FunctionPassManager> TheFPM;
std::map<std::string, AllocaInst *> variables;
std::map<std::string, Argument *> arguments;

namespace codegen
{
    void create_variable(unique_tok identifier_tok, Type *type, Value *initial_value)
    {
        if (variables[identifier_tok->value] || arguments[identifier_tok->value])
        {
            CodeGenerationError<AllocaInst *>((std::string() + '"' + identifier_tok->value + '"' + " is already defined").c_str());
            return;
        }
        AllocaInst *variable = Builder->CreateAlloca(type, nullptr, identifier_tok->value);
        variables[identifier_tok->value] = variable;

        if (initial_value)
            Builder->CreateStore(initial_value, variable);
    }

    Value *load_variable(const std::string &name)
    {
        auto variable = variables[name];

        if (variable)
            return Builder->CreateLoad(variable);

        auto argument = arguments[name];
        if (argument)
            return argument;

        return CodeGenerationError<Value *>((std::string() + '"' + name + "\" is not defined").c_str());
    }

    void store_variable(const std::string &name, Value *value)
    {
        auto variable = variables[name];

        if (variable)
        {
            Builder->CreateStore(value, variable);
            return;
        }

        auto argument = arguments[name];
        if (argument)
        {
            Builder->CreateStore(value, argument);
            return;
        }

        CodeGenerationError<Value *>((std::string() + '"' + name + "\" is not defined").c_str());
    }
}

void InitialzeModule()
{
    TheContext = std::make_unique<LLVMContext>();
    TheModule = std::make_unique<Module>("my cool jit", *TheContext);
    TheFPM = std::make_unique<legacy::FunctionPassManager>(TheModule.get());

    // TheFPM->add(createInstructionCombiningPass());
    // TheFPM->add(createReassociatePass());
    // TheFPM->add(createGVNPass());
    // TheFPM->add(createCFGSimplificationPass());
    // TheFPM->doInitialization();

    Builder = std::make_unique<IRBuilder<>>(*TheContext);
}